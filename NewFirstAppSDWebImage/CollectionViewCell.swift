import UIKit
import SDWebImage

class CollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imageView: UIImageView!
    
    var product:Product?
    
    func setShowInformation(_ product:Product) {
        
        self.product = product
        
        
        let url = URL.init(string: product.img!)
        self.imageView.sd_setImage(with: url)

    }    
}
