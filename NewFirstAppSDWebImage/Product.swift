import Foundation

class Product: NSObject {
    
    var w:Int = 0
    var h:Int = 0
    var price:String?
    var img:String?
    
    
    static func getProduct(_ dictionary:NSDictionary ) -> Product {
        let product = Product.init()
        product.setValuesForKeys(dictionary as! [String : AnyObject])
        return product
    }
    
    
    static func getDataForProducts(_ index:Int8) -> NSArray {
        let fileName = "\(index + 1).plist"
        let path = Bundle.main.path(forResource: fileName, ofType: nil)
        let contentsOfFile = NSArray.init(contentsOfFile: path!)
        let products = contentsOfFile?.map{self.getProduct($0 as! NSDictionary)}
        return products! as NSArray
    }
    
}
