import UIKit

class CollectionViewFlowLayout: UICollectionViewFlowLayout {
    var products = [Product]()
    var columnCount:Int = 0
    
    fileprivate var layoutAttributesArrayForItems = [UICollectionViewLayoutAttributes]()
    
    override func prepare() {
        super.prepare()
        minimumInteritemSpacing = 10
        minimumLineSpacing = 10
        
        sectionInset.top = 10
        sectionInset.left = 10
        sectionInset.right = 10
        
        let contentWidth:CGFloat = (self.collectionView?.bounds.size.width)! - sectionInset.left - sectionInset.right
        let interitemSpacingSum = self.minimumLineSpacing * CGFloat.init(columnCount - 1)
        
        let itemWidth = (contentWidth - interitemSpacingSum) / CGFloat.init(columnCount)
        
        var columnsHeight = [CGFloat](repeating: CGFloat(self.sectionInset.top), count: self.columnCount)
        
        var columnItemCount = [Int](repeating: 0, count: self.columnCount)
        
        var attributesForItems = [UICollectionViewLayoutAttributes]()
        var itemIndex = 0
        for product in products {
            let indexPath = IndexPath.init(item: itemIndex, section: 0)
            let attributes = UICollectionViewLayoutAttributes.init(forCellWith: indexPath)
            let shortestColumnHeight = columnsHeight.sorted().first
            let columnIndex = columnsHeight.index(of: shortestColumnHeight!)
            
            columnItemCount[columnIndex!] += 1
            
            let itemX = (itemWidth + CGFloat(minimumLineSpacing)) * CGFloat(columnIndex!) + sectionInset.left
            
            let itemY = shortestColumnHeight! + CGFloat(minimumLineSpacing)
            let itemHeight = CGFloat(product.h) * (itemWidth / CGFloat(product.w))
            
            attributes.frame = CGRect(x: itemX, y: itemY, width: itemWidth, height: itemHeight)
            
            attributesForItems.append(attributes)
            itemIndex += 1
            
            columnsHeight[columnIndex!] = itemY + itemHeight
        }
        

        let valueForMaxColumnHeight = columnsHeight.sorted().last!
        let columnOfMaxHeight = columnsHeight.index(of: valueForMaxColumnHeight)
        
        // 根据最高列设置itemSize 使用总高度的平均值
        let itemSizeHeight = (valueForMaxColumnHeight - self.minimumLineSpacing * CGFloat(columnItemCount[columnOfMaxHeight!])) / CGFloat(columnItemCount[columnOfMaxHeight!])
        
        self.itemSize = CGSize(width: itemWidth, height: itemSizeHeight)
        // 添加页脚属性
        let footerIndexPath:IndexPath = IndexPath.init(item: 0, section: 0)
        let footerLayoutAttributes:UICollectionViewLayoutAttributes = UICollectionViewLayoutAttributes.init(forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, with: footerIndexPath)
        footerLayoutAttributes.frame = CGRect(x: 0, y: CGFloat(valueForMaxColumnHeight), width: self.collectionView!.bounds.size.width, height: 50)
        attributesForItems.append(footerLayoutAttributes)
        
        
        self.layoutAttributesArrayForItems = attributesForItems
        
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return self.layoutAttributesArrayForItems
    }
}
