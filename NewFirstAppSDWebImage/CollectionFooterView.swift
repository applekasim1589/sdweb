import UIKit

class CollectionFooterView: UICollectionReusableView {
    @IBOutlet weak var indicator: UIActivityIndicatorView!
}
