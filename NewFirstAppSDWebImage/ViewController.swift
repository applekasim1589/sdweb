//
//  ViewController.swift
//  NewFirstAppSDWebImage
//
//  Created by 周政翰 on 2017/6/1.
//  Copyright © 2017年 kasim. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var products = [Product]()
    var index:Int8 = 0
    var labelIndex = 0
    var loading = false
    
    var footerView:CollectionFooterView!
    
    @IBOutlet var flowLayout : CollectionViewFlowLayout!
    @IBOutlet var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadData()
    }
    
    func loadData() {
        let dataOfProducts = Product.getDataForProducts(self.index)
        self.products.append(contentsOf: dataOfProducts as! [Product])
        self.index += 1
        
        self.flowLayout.columnCount = 3;
        self.flowLayout.products = self.products
        self.collectionView.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if self.footerView == nil || self.loading == true {
            return
        }
        
        if self.index > 2 {
            self.footerView.isHidden = true
            return
        }
        
        if self.footerView!.frame.origin.y < (scrollView.contentOffset.y + scrollView.bounds.size.height) {
        
        self.loading = true
        self.footerView?.indicator.startAnimating()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1 * NSEC_PER_SEC)) / Double(NSEC_PER_SEC), execute: {
               self.footerView = nil
               self.loadData()
               self.loading = false
           
               }
           )
        }
        
    }
}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.products.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as! CollectionViewCell
        
        cell.setShowInformation(self.products[(indexPath as NSIndexPath).item])
        
        labelIndex += 1
        
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionFooter {
            self.footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FooterView", for: indexPath) as? CollectionFooterView
            return self.footerView!
        }
        
        assert(false, "Unexpected element kind")
    }
}

